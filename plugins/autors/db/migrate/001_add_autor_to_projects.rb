class AddAutorToProjects < ActiveRecord::Migration

  def self.up
    add_column :projects, :autor, :integer
  end

  def self.down
    remove_column :projects, :autor
  end
end
