# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

get '/autors', to: 'autors#edit'
post '/autors', to: 'autors#update'
patch '/autors', to: 'autors#update'
