Redmine::Plugin.register :autors do
  name 'Autors plugin'
  author 'Author name'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'
  permission :autors, { autors: [:index, :edit, :update] }, public: true
  menu :project_menu, :autors, { controller: 'autors', action: 'edit' },
                    caption: 'Autors', after: :activity, params: :project_id
end
