class AutorsController < ApplicationController
  #unloadable


  def index

  end

  def edit
    @project = Project.find(params[:id])
    @users = User.where('login != ""') # I hate you anon
    if @project.autor
      @autor = User.find(@project.autor)
    else
      @autor = User.new
    end
  end

  def update
    if params[:user][:id]
      @project = Project.find(params[:project_id])
      @project.update_attribute(:autor, params[:user][:id])

    end
    redirect_to controller: 'autors', action: 'edit', id: params[:project_id]
  end
end
